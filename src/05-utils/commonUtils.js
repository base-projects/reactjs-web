import queryString from 'querystring';
import 'isomorphic-fetch';

const parseUrlQuery = (query) => {
	if (query.indexOf('?') >= 0) query = query.substr(query.indexOf('?') + 1);
	return queryString.parse(query);
};

const callApi = (url,
                 config,
                 onRequestFinish,
                 onRequestSuccess,
                 onRequestFailure) => {
	
	return fetch(url, config)
		.then(response => {
			onRequestFinish && onRequestFinish();
			if (config.isFileDownload) {
				onRequestSuccess && onRequestSuccess(response);
				return null;
			}
			else {
				return response.json();
			}
		})
		.then((data) => {
			if (data && !config.isFileDownload) {
				data.success && onRequestSuccess && onRequestSuccess(data.result);
				!data.success && onRequestFailure && onRequestFailure(data.error);
			}
		})
		.catch((err) => {
			onRequestFinish && onRequestFinish();
			onRequestFailure && onRequestFailure({code: 'NETWORK_ERROR'});
		});
};

export {callApi, parseUrlQuery};