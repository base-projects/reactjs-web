import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';

import auth from './02-reducers/auth';

const initialState = {};

export default () => {
	return createStore(
		combineReducers({
			auth,
		}),
		initialState,
		applyMiddleware(thunkMiddleware)
	);
}