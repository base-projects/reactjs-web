import React, {PureComponent} from 'react';
import {
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from 'reactstrap';

export default class Alert extends PureComponent {
	constructor(props) {
		super(props);
		this.handleClose = this.handleClose.bind(this);
	}
	
	handleClose() {
		this.props.onClose();
	}
	
	render() {
		return (
			<Modal isOpen={true}>
				<ModalHeader>{this.props.header}</ModalHeader>
				<ModalBody>{this.props.content}</ModalBody>
				<ModalFooter>
					{this.props.cancelTitle && (
						<Button>{this.props.cancelTitle}</Button>
					)}
					<Button onClick={this.handleClose}>{this.props.okTitle}</Button>
				</ModalFooter>
			</Modal>
		);
	}
	
}