import {getEnv} from "../env";
import {callApi} from "../05-utils/commonUtils";

const ACTIONS = {
	LOGIN_PROGRESS: "LOGIN_PROGRESS",
	LOGIN_SUCCESS: "LOGIN_SUCCESS",
	LOGIN_FAILED: "LOGIN_FAILED",
	REGISTER_PROGRESS: "REGISTER_PROGRESS",
	REGISTER_SUCCESS: "REGISTER_SUCCESS",
	REGISTER_FAILED: "REGISTER_FAILED",
	LOGOUT: "LOGOUT",
};

const updateStatus = (status, statusInfo) => {
	return {
		type: status,
		data: statusInfo
	}
};

const login = (loginData) => {
	let config = {
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			grantType: 'password',
			grantData: [loginData.username, loginData.password]
		})
	};
	
	return (dispatch) => {
		dispatch(updateStatus(ACTIONS.LOGIN_PROGRESS));
		callApi(getEnv('REACT_APP_API_SERVER') + '/api/oauth/token',
			config,
			null,
			(data) => {
				dispatch(updateStatus(ACTIONS.LOGIN_SUCCESS, {user: data}));
			},
			(err) => {
				dispatch(updateStatus(ACTIONS.LOGIN_FAILED, {error: err}));
			}
		);
	};
};

const register = (registerData) => {
	let config = {
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			username: registerData.username,
			authMethod: 'password',
			authInfo: [registerData.password]
		})
	};
	
	return (dispatch) => {
		dispatch(updateStatus(ACTIONS.REGISTER_PROGRESS));
		callApi(getEnv('REACT_APP_API_SERVER') + '/api/register',
			config,
			null,
			() => {
				dispatch(updateStatus(ACTIONS.REGISTER_SUCCESS));
			},
			(err) => {
				dispatch(updateStatus(ACTIONS.REGISTER_FAILED, {error: err}));
			}
		);
	};
};

const logout = () => {
	return {
		type: ACTIONS.LOGOUT
	}
};

export {ACTIONS, login, register, logout};