This ReactJs web template is created by Facebook create-react-app, plus:
- `react-router-dom` for routing
- `redux` for state management
- `redux-thunk` for async state update
- `react-localize-redux` for multi language support
- `reactstrap` for bootstrap 4 components
- `getEnv` and CI/CD ready
- **scss** support

The project has been structured for a multi-pages application with different layouts.
Basic authentication routing has been applied using react-router-dom `Redirect`.